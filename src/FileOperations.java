import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {


    public static List<String> readPopularWebsites(String name) {

        List<String> websiteRecords = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(name))) {
            String line;
            while ((line = br.readLine()) != null) {

                // String format in .csv file is:
                // "1","youtube.com","View analysis","21,409,398","100"
                // "2","www.google.com","View analysis","13,559,229","100"

                String[] values = line.split(",");
                // Keep the "youtube.com" part of the original string
                String websiteStringWithQuotes = values[1];
                // Get rid of the quotes ("") and then get rid of "www." if present
                String[] value = websiteStringWithQuotes.split("\"");
                String webString = value[1].replaceAll("www.", "");
                // Add the domain name to the webString
                websiteRecords.add((String) webString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return websiteRecords;
    }


    public static List<String> readUserVisitedWebsites(String name) {

        List<String> websiteRecords = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(name))) {
            String line;
            while ((line = br.readLine()) != null) {
                websiteRecords.add((String) line);
                // For Debugging purposes
                //System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return websiteRecords;
    }


    public static void writeUserVisitedWebsite(String name) {

        String filename= "findUserDomains.txt";
        try (BufferedWriter fw = new BufferedWriter(new FileWriter(filename, true))) {
            fw.newLine();
            fw.write(name);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


}
