public class TypoFinder {


    public static boolean missingOneCharacter(String original, String tested) {

        //System.out.println(original + " --> " + tested);

        if (original.length() - tested.length() != 1) {
            return false;
        } else {

            int counter = 0;
            int k = 0;
            int i = 0;

            for (i = 0; i < tested.length(); i++) {

                if (tested.charAt(i) != original.charAt(k)) {
                    //System.out.println("_" + " --> " + original.charAt(k));
                    counter++;
                    // Check the next char in the original word.
                    // If no match is found --> break
                    k++;
                    if(counter == 2){
                        return false;
                    }
                    if (tested.charAt(i) != original.charAt(k)) {
                        counter++;
                        return false;
                    }
                }
                // For Debugging purposes
                //System.out.println(tested.charAt(i) + " --> " + original.charAt(k) );
                k++;
            }

            return true;
        }
    }


    public static boolean extraOneCharacter(String original, String tested) {
        return missingOneCharacter(tested, original);
    }


    public static boolean replacedCharacter(String original, String tested) {

        if (original.length() != tested.length()) {
            return false;
        } else {
            int counter = 0;
            for (int i = 0; i < tested.length(); i++) {
                if (tested.charAt(i) != original.charAt(i)) {
                    counter++;
                }
            }
            if (counter == 1) {
                return true;
            } else {
                return false;
            }
        }
    }


    public static boolean adjacentSwappedCharachters(String original, String tested) {

        if (original.length() != tested.length()) {
            return false;
        } else {
            int counter = 0;
            int pos1 = -2;
            int pos2 = -2;
            for (int i = 0; i < tested.length(); i++) {
                if (tested.charAt(i) != original.charAt(i)) {
                    counter++;
                    if (pos1 < 0) {
                        pos1 = i;
                    } else {
                        pos2 = i;
                    }
                }
            }
            // For Debugging purposes
            //System.out.println("counter = " + counter + " pos1 = " + pos1 + " pos2 = " + pos2);
            if (counter > 2) {
                return false;
            } else {
                if (pos2 - pos1 == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }


    public static boolean typosquatting(String original, String tested) {

        if ((replacedCharacter(original, tested) == true) ||
                (missingOneCharacter(original, tested) == true) ||
                (extraOneCharacter(original, tested) == true) ||
                (adjacentSwappedCharachters(original, tested) == true)
            ) {
                return true;
            } else {
                return false;
            }
        }


    }




